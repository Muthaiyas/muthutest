﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJackGame
{
    // Range FaceValues 1 to 10
    public enum face
    {
        One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten

    }
   public  class FaceValues
    {
        public face face { get; set; }
        public int Values { get; set; }

    }
}
