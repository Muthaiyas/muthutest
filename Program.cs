﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlackJackGame
{
    class Program
    {
        static RandomGeneration randomgen;
        static List<FaceValues> PlayerHand;
        static List<FaceValues> ComputerHand;

        //Main Function Of BlackJack Game
        static void Main(string[] args)
        {
            //Method Calling
            randomgen = new RandomGeneration();
            randomgen.Shuffle();
            LogicBlcakJack();
            Console.WriteLine("\nPress any key for the next hand...\n");
            ConsoleKeyInfo userInput = Console.ReadKey(true);
            Console.WriteLine("see you next time...");
            Console.ReadLine();
        }

          static void LogicBlcakJack()
        {
            Console.WriteLine();

            PlayerHand = new List<FaceValues>();
            PlayerHand.Add(randomgen.DrawACard());
            PlayerHand.Add(randomgen.DrawACard());
              
            foreach (FaceValues FaceValues in PlayerHand)
            {

                if (FaceValues.face == face.One)
                {
                    FaceValues.Values += 0;
                    break;
                }

                //Player Card Values Displayed here
                Console.WriteLine("[Your Card]");
                //Player First Card Value
                Console.WriteLine("Your Card 1: {0}", PlayerHand[0].face);
                //Player Second Card Value
                Console.WriteLine("Your Card 2: {0}", PlayerHand[1].face);
                // Two Card Total Value
                Console.WriteLine("Total: {0}\n", PlayerHand[0].Values + PlayerHand[1].Values);

                ComputerHand = new List<FaceValues>();
                ComputerHand.Add(randomgen.DrawACard());
                ComputerHand.Add(randomgen.DrawACard());

                foreach (FaceValues Face in ComputerHand)
                {
                    if (FaceValues.face == face.One)
                    {
                        FaceValues.Values += 0;
                        break;
                    }
                }

                //Computer Card Value Displayed here
                Console.WriteLine("[Computer Card]");
                //Player Card Values Displayed here
                Console.WriteLine("Computer Card 1: {0}", ComputerHand[0].face);
                //Player Second Card Value
                Console.WriteLine("Computer Card 2: {0}", ComputerHand[1].face);
                // Two Card Total Value
                Console.WriteLine("Total: {0}\n", ComputerHand[0].Values + ComputerHand[1].Values);

                //Computer Checks blackjack logic
                if (ComputerHand[0].face == face.One || ComputerHand[0].Values == 0)
                {
                    Console.WriteLine("Computer checks if he has blackjack...\n");
                    if (ComputerHand[0].Values + ComputerHand[1].Values == 21)
                    {
                        Console.WriteLine("[Computer]");
                        Console.WriteLine("Card 1: {0}", ComputerHand[0].face);
                        Console.WriteLine("Card 2: {0}", ComputerHand[1].face);
                        Console.WriteLine("Total: {0}\n", ComputerHand[0].Values + ComputerHand[1].Values);
                    }

                }

                //Opportunity to take additional card

                do
                {
                    Console.WriteLine("Do you want another card: [(Y)es (N)o]");
                    // Reads Useroperation
                    ConsoleKeyInfo userOption = Console.ReadKey(true);

                    //If we Press other than (Y/N)

                    while (userOption.Key != ConsoleKey.Y && userOption.Key != ConsoleKey.N)
                    {
                        Console.WriteLine("illegal key. Please choose a valid option: [(Y)es (N)o]");
                        userOption = Console.ReadKey(true);
                    }
                    Console.WriteLine();

                    //Case statement
                    switch (userOption.Key)
                    {
                        // If we select 'Yes' to take additional card
                        case ConsoleKey.Y:
                            PlayerHand.Add(randomgen.DrawACard());
                            Console.WriteLine("Yes {0}", PlayerHand[PlayerHand.Count - 1].face);
                            int totalCardsValue = 0;
                            foreach (FaceValues FaceValues1 in PlayerHand)
                            {
                                totalCardsValue += FaceValues1.Values;
                            }
                            Console.WriteLine("Hit :Total cards value now: {0}\n", totalCardsValue);
                            // CardValue greater than 21
                            if (totalCardsValue > 21)
                            {
                                Console.Write("Lose!\n");
                                return;
                            }
                            //TotalcardValue equal to 21
                            else if (totalCardsValue == 21)
                            {
                                Console.WriteLine("Wow ! You Won\n");
                                //continue;
                                return;

                            }
                            else
                            {
                                continue;
                            }

                        // If we select 'No' computer have to take additional card
                        case ConsoleKey.N:

                            Console.WriteLine("[Computer]");
                            Console.WriteLine("Card 1: {0}", ComputerHand[0].face);
                            Console.WriteLine("Card 2: {0}", ComputerHand[1].face);
                            int computerCardsValue = 0;
                            foreach (FaceValues FaceValues2 in ComputerHand)
                            {
                                computerCardsValue += FaceValues2.Values;
                            }


                            while (computerCardsValue < 17)
                            {

                                ComputerHand.Add(randomgen.DrawACard());
                                computerCardsValue = 0;
                                foreach (FaceValues FaceValues3 in ComputerHand)
                                {
                                    computerCardsValue += FaceValues3.Values;
                                }
                                Console.WriteLine("Card {0}: {1}", ComputerHand.Count, ComputerHand[ComputerHand.Count - 1].face);
                            }
                            computerCardsValue = 0;
                            foreach (FaceValues FaceValues4 in ComputerHand)
                            {
                                computerCardsValue += FaceValues4.Values;
                            }
                            Console.WriteLine("Total: {0}\n", computerCardsValue);

                            if (computerCardsValue > 21)
                            {
                                Console.WriteLine(" You Won! ({0} chips)");
                                return;
                            }
                            else
                            {
                                int yourCardvalue = 0;
                                foreach (FaceValues FaceValues6 in PlayerHand)
                                {
                                    yourCardvalue += FaceValues6.Values;
                                }
                                //Result of the game
                                if (computerCardsValue == yourCardvalue)
                                {
                                    Console.WriteLine("Computer has {0} and You has {1}, Scores Level", computerCardsValue, yourCardvalue);

                                    return;
                                }
                                if (computerCardsValue > yourCardvalue)
                                {
                                    Console.WriteLine("Computer has {0} and You has {1}, Computer Won!", computerCardsValue, yourCardvalue);

                                    return;
                                }
                                else
                                {
                                    Console.WriteLine("You has {0} and Computer has {1}, You Won!", yourCardvalue, computerCardsValue);

                                    return;
                                }
                            }


                        default:
                            break;
                    }

                    Console.ReadLine();
                }
                while (true);
            }
        }
    }
}
