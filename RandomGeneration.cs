﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BlackJackGame
{
    public class RandomGeneration
    {
        private List<FaceValues> FaceValue;

        public RandomGeneration()
        {

            this.RandomGen();
        }

        //Random Number Generation range 1 to 10
        public void RandomGen()
        {
            FaceValue = new List<FaceValues>();
            {
                for (int j = 0; j < 10; j++)
                {
                    FaceValue.Add(new FaceValues() { face = (face)j });
                    if (j <= 9)
                    {
                        FaceValue[FaceValue.Count - 1].Values = j + 1;
                    }
                }
            }

        }

        //Shuffle the Values
        public void Shuffle()
        {
            Random rng = new Random();
            int n = FaceValue.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                FaceValues FaceValues = FaceValue[k];
                FaceValue[k] = FaceValue[n];
                FaceValue[n] = FaceValues;
            }
        }
        public FaceValues DrawACard()
        {
            RandomGeneration rand = new RandomGeneration();
            rand.RandomGen();
            this.Shuffle();
            FaceValues cardToReturn = FaceValue[FaceValue.Count - 1];
            FaceValue.RemoveAt(FaceValue.Count - 1);
           
            return cardToReturn;
        }
        public void PrintDeck()
        {
            int i = 1;
            foreach (FaceValues FaceValues in FaceValue)
            {
                Console.WriteLine("Card {0}: {1} of {2}. Value: {3}", i, FaceValues.face, FaceValues.face);
                i++;
            }
        }
    }
}


